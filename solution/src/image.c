//
// Created by Максим on 29.12.2021.
//
#include "image.h"

#include <inttypes.h>
#include <malloc.h>

struct image init_image(uint32_t width, uint32_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height),
    };
}

bool set_pixel(struct image *image, uint32_t x, uint32_t y, struct pixel pixel) {
    if ((x >= image->width) || (y >= image->height)) return false;
    image->data[y * (image->width) + x] = pixel;
    return true;
}

struct pixel get_pixel(const struct image *image, uint32_t x, uint32_t y) {
    if ((x > image->width) || (y > image->height)) {
        return (struct pixel) {0};
    }
    return (image->data)[y * (image->width) + x];
}

void delete_image(struct image *image) {
    free(image->data);
}
