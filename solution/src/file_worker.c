//
// Created by Максим on 29.12.2021.
//

#include "file_worker.h"
#include <errno.h>
#include <stdio.h>


enum file_work_result open_file(const char *path, FILE **file, const char *mode) {
    if (!path) return FILE_INCORRECT_PATH;
    *file = fopen(path, mode);
	if (errno == ENOENT) return FILE_NOT_EXIST;
	if (errno == EACCES) return FILE_PERMISSION_DENIED;

    return FILE_OK;

}

enum file_work_result close_file(FILE *file) {
    if (!file) return FILE_NOT_EXIST;
    if (fclose(file)) return FILE_PERMISSION_DENIED;
    
    return FILE_OK;
}
