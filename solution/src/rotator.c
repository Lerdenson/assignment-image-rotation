//
// Created by Максим on 29.12.2021.
//

#include "rotator.h"

#include <stdint.h>

struct image rotate(const struct image *source_image) {
    struct image result_image = init_image(source_image->height, source_image->width);

    for (int32_t i = 0; i < source_image->height; i++) {
        for (int32_t j = 0; j < source_image->width; j++) {
            set_pixel(
                &result_image,
                i, j, 
                get_pixel(source_image, j, source_image->height - i - 1)
                );
        }
    }
    return result_image;

}
