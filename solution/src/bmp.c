//
// Created by Максим on 29.12.2021.
//

#include "bmp.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

#define BFTYPE 0x4D42
#define HEADER_SIZE 54
#define INFO_HEADER_SIZE 40
#define PLANES_NUMBER 1
#define PIXEL_SIZE 24

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool validate_header_signature(const struct bmp_header header) {
    return header.bfType == BFTYPE;
}

static bool test_padding(const uint32_t width) {
    return (width * 3) % 4;
}

static uint32_t calculate_padding(uint32_t width) {
    if (test_padding(width)) {
        return 4 - ((width * 3) % 4);
    } else {
        return 0;
    }
}

static bool write_padding(FILE* out, const uint32_t width) {
    int8_t zero = 0;
    for (size_t j = 0; j < calculate_padding(width); j++){
        if(!fwrite(&zero, sizeof (int8_t), 1, out)) return false;
    }
    return true;
}

static bool to_image ( FILE* in, const struct bmp_header header, struct image* img) {
    if (header.biHeight <= 0 || header.biWidth <= 0) return false;
    *img = init_image(header.biWidth, header.biHeight);
    fseek(in, header.bOffBits, SEEK_SET);
    struct pixel* line = NULL;
    for (int32_t i = 0; i < img->height; i++) {
        line = img->data + i * img->width;

        if (fread(line, sizeof (struct pixel), img->width, in) != img->width) {
            delete_image(img);
            return false;
        }

        if (test_padding(img->width)) {
            fseek(in, calculate_padding(img->width), SEEK_CUR);
        }
    }
    return true;
}

static struct bmp_header create_header(const struct image* img) {
    uint32_t FILE_SIZE = sizeof (struct bmp_header)
        + img->width * img->height * sizeof(struct pixel)
        + img->height * calculate_padding(img->width);

    return (struct bmp_header) {
    .bfType = BFTYPE,
    .bfileSize = FILE_SIZE,
    .bOffBits = HEADER_SIZE,
    .biSize = INFO_HEADER_SIZE,
    .biWidth = img->width,
    .biHeight = img->height,
    .biPlanes = PLANES_NUMBER,
    .biBitCount = PIXEL_SIZE,
    .biSizeImage = FILE_SIZE - HEADER_SIZE
    };
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_HEADER;
    if (!validate_header_signature(header)) return READ_INVALID_SIGNATURE;
    if (!to_image(in, header, img)) return READ_INVALID_BITS;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = create_header(img);
    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) return WRITE_ERROR;
    struct pixel* line = NULL;
    for (int32_t i = 0; i < img->height; i++){
        line = img->data + i * img->width;
        if (!fwrite(line, sizeof (struct pixel), img->width, out)) return WRITE_ERROR;

        if(!write_padding(out, img->width)) return WRITE_ERROR;
    }
    return WRITE_OK;
}

