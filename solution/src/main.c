#include "bmp.h"
#include "file_worker.h"
#include "image.h"
#include "rotator.h"

#include <stdio.h>

int main(int argc, char *argv[]) {

    if (argc != 3) {
        printf("Incorrect input\n");
        return 0;
    }

    FILE* file = NULL;

    if(open_file(argv[1], &file, "rb")) {
        printf("Can't open file");
        return 0;
    }

    struct image source_img = {0};

    switch (from_bmp(file, &source_img)) {
        case READ_INVALID_BITS:
        case READ_INVALID_SIGNATURE:
        case READ_INVALID_HEADER:
            printf("Invalid bmp");
            close_file(file);
            delete_image(&source_img);
            return 0;
        case READ_OK:
            break;
    }

    if(close_file(file)) {
        printf("Can't close file");
        delete_image(&source_img);
        return 0;
    }


    struct image result_img = rotate(&source_img);
    FILE* result_file = NULL;

    if(open_file(argv[2], &result_file, "wb")) {
        printf("Can't open result file");
        delete_image(&source_img);
        delete_image(&result_img);
        return 0;
    }

    if (to_bmp(result_file, &result_img)) {
        printf("Can't create bmp");
        delete_image(&source_img);
        delete_image(&result_img);
        close_file(result_file);
        return 0;
    }

    delete_image(&source_img);
    delete_image(&result_img);

    if (close_file(result_file)) {
        printf("Can't close result file");
        return 0;
    }

    return 0;
}

