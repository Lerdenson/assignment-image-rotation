//
// Created by Максим on 29.12.2021.
//
#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image init_image(uint32_t width, uint32_t height);
bool set_pixel(struct image* image, uint32_t x, uint32_t y, struct pixel pixel);
struct pixel get_pixel(const struct image* image, uint32_t x, uint32_t y);
void delete_image(struct image* image);

#endif //LAB3_IMAGE_H
