//
// Created by Максим on 29.12.2021.
//

#ifndef LAB3_ROTATOR_H
#define LAB3_ROTATOR_H

#include "image.h"

struct image rotate(const struct image *source_image);

#endif //LAB3_ROTATOR_H
