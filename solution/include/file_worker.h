//
// Created by Максим on 29.12.2021.
//

#ifndef LAB3_FILE_WORKER_H
#define LAB3_FILE_WORKER_H

#include <stdbool.h>
#include <stdio.h>

enum file_work_result {
	FILE_OK = 0,
	FILE_PERMISSION_DENIED,
    FILE_NOT_EXIST,
	FILE_INCORRECT_PATH
};


enum file_work_result open_file(const char* path, FILE** file, const char* mode);
enum file_work_result close_file(FILE* file);

#endif //LAB3_FILE_WORKER_H
